#include "threads.h"
#include <thread>
#include <time.h>

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	thread(I_Love_Threads).join();
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	for (int i = begin; i < end; i++)
	{
		bool prime = true;
		for (int j = 2; j < i; j++)
		{
			if (i % j == 0)
			{
				prime = false;
				break;
			}
		}

		if (prime)
		{
			primes.push_back(i);
		}
	}
}

void printVector(vector<int> primes)
{
	for (int i : primes)
	{
		cout << i << endl;
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int>* primes = new vector<int>();
	time_t start = time(0);
	thread(getPrimes, begin, end, ref(*primes)).join();
	cout << "Primes from " << begin << " to " << end << "'s Time: " << difftime(time(0), start) << endl;
	return *primes;
}

void writePrimesToFile(int begin, int end, ofstream& file)
{
	for (int i = begin; i < end; i++)
	{
		bool prime = true;
		for (int j = 2; j < i; j++)
		{
			if (i % j == 0)
			{
				prime = false;
				break;
			}
		}

		if (prime)
		{
			file << i << endl;
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	time_t start = time(0);
	ofstream file;
	file.open(filePath);
	int size = (end - begin) / N;
	int b = begin;
	for (int n = 0; n < N; n++)
	{
		thread(writePrimesToFile, begin, begin + size, ref(file)).join();
		begin += size;
	}
	file.close();
	cout << "Primes from " << b << " to " << end << "'s time (in file): " << difftime(time(0), start) << endl;
}

